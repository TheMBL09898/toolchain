# toolchain: statically linked self contained toolchains

This repository aims to provide fully self-contained toolchains. As of now there is only Clang/LLVM, but of course I would love to add GCC toolchains as well like [musl.cc](https://musl.cc). Theirs are somewhat outdated, and have some issues, for example the aarch64-linux-musl-cross toolchain I used recently is built in 32bit (wtf).

An aim of this repo is to make all the provided toolchains 100% compatible with [oasislinux](https://codeberg.org/Potosi/oasis), so more alternatives are present other than the only one mcf provides (although musl.cc still exists).

# Building a statically linked clang toolchain

Welcome!! This repo will show you how to bootstrap an optimized and statically linked LLVM toolchain, with no dependencies on GCC and easy support for cross compiling to many cpu architectures and libc implementations.

## Resources

I was suffering trying to build Clang statically till I found these and just forked the containers repo:

* [ClangBuiltLinux](https://github.com/ClangBuiltLinux)
    * [containers](https://github.com/ClangBuiltLinux/containers)
    * [tc-build](https://github.com/ClangBuiltLinux/tc-build)
    * [figure out how to statically link clang itself #150](https://github.com/ClangBuiltLinux/tc-build/issues/150)

## Goals

* - [ ] Allows easy cross compiling
* - [x] Is portable and self-contained (static)
* - [x] Works independently of other projects outside LLVM (libgcc...)
* - [ ] Support for many host architectures. (arm, RISC-V)
* - [ ] Works across multiple Operating Systems (Cosmopolitan libc - Maybe in the far future)
* - [ ] Mega-optimized (PGO, LTO, BOLT)
* - [ ] Includes other LLVM compilers outside of the LLVM project (Rust, Haskell)

As of now the provided toolchain in branch "clang-x86_64-unknown-linux-musl-native" works, and is independent of libgcc and other software not built by clang. It is a stage3 build. Now that the initial build is finished, a number of things are in my mind right now:

* Compile the whole of oasis for clang (fix deps on lgcc and warnings)
* Build stage3 with llvm-install-symlinks
* Perhaps just install everything?
* Use oasis-sysroots for stage3
* Build stage3 with LTO (sizes too large)
* After try with PGO and Bolt (stage 4 - see attempts from fork)
* Add CI for automatic building and Runner
* Add package
* Modify oasislinux-build deploy script
* Add support for more targets (cross compiling to glibc)

It's not absolutely perfect yet but it should work properly now. This is here to show it's just not finished yet.

## Clang (and GCC) Suck

Before starting with this guide, It is my duty to remember you that the sole reason you are here is because Clang sucks. Pretty bad. Imagine having to look up a random guy's codeberg to see how to statically link a compiler. Software shouldn't be this complex, even talking about compilers. And GCC is no better either. To sum it up, here is a quote from suckless's website:

> Clang is written in C++. If you don't believe that it sucks, try to build clang by hand.

You will get the point pretty soon.

## Why LLVM

Because nobody's really done that before, and it is interesting as an investigation project. CMake still sucks, Clang is also shit, but there are some kinds of advantages over GCC (although we strongly advise against using bloated software nevertheless).

1. Clang supports LTO when statically linked, as opposed to GCC, which uses dlopen to perform this kind of optimization. This means that we could use Clang as our compiler in [oasislinux](https://codeberg.org/Potosi/oasis). I realize there's literally no point in doing this, ok?
2. Cross compiling support is better (not really)
3. Probably outperforms GCC at build speed (nobody cares)
4. The flex?? (JUST FOR NORMIES - REAL BROS USE TCC OR CPROC)
5. MOAR OPTIMIZATIONSS (Bolt, ThinLTO...)

## Objectively good compilers

Jokes aside, here are the compilers you should be using right now:

* [cproc](https://github.com/) An optimizing, fast and incredible simple C compiler.
* [tcc](https://bellard.org/tcc/) A classic: historically one of the fastest compilers of all time.

These are the only ones which I have tried, there are much more on [suckless' "rocks" page.](https://suckless.org/rocks)

Some of the content is forked from [ClangBuiltLinux's containers](https://github.com/ClangBuiltLinux/containers). Their README lives at README-ClangBuiltLinux.md

# Howto

You can get my prebuilts, which is far easier than building everything yourself. Each toolchain lives in a branch corresponding to the following format: "<toolchain>-<target-triple>-<type>", e.g "clang-x86_64-unknown-linux-musl-native".

## Building

I provide a simple Makefile (forked) so you don't have to suffer too much. Simply run "make" and the toolchain will bootstrap itself in docker containers. Read README-ClangBuiltLinux.md for more.
